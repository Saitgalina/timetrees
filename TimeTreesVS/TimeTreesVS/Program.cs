﻿using System;
using System.IO;
using System.Globalization;
using Newtonsoft.Json;

namespace TimeTreesVS
{
    struct Person
    {
        public int id;
        public string name;
        public DateTime birthday;
        public DateTime deathday;
    }

    struct TimelineEvent
    {
        public DateTime date;
        public string description;
    }
    class Program
    {
        const int idIndex = 0;
        const int nameIndex = 1;
        const int birthDayIndex = 2;
        const int deathDayIndex = 3;
        const int timelineDateIndex = 0;
        const int descriptionIndex = 1;

        static Person[] ReadPeople(string file)
        {
            string[] peopleLines = File.ReadAllLines(file);
            int n = peopleLines.Length;
            Person[] persons = new Person[n];
            
            for (int i = 0; i < n; i++)
            {
                string[] temp = peopleLines[i].Split(";");
                persons[i].id = int.Parse(temp[idIndex]);
                persons[i].name = temp[nameIndex];
                persons[i].birthday = ParseDate(temp[birthDayIndex]);
                if (temp.Length == 4)
                {
                    persons[i].deathday = ParseDate(temp[deathDayIndex]);
                }
            }
            return persons;
        }
        static TimelineEvent[] ReadTimeline(string file)
        {
            string[] timeLines = File.ReadAllLines(file);
            int n = timeLines.Length;
            TimelineEvent[] events = new TimelineEvent[n];

            for (int i = 0; i < n; i++)
            {
                string[] temp = timeLines[i].Split(";");
                events[i].date = ParseDate(temp[timelineDateIndex]);
                events[i].description = temp[descriptionIndex];
            }
            return events;
        }
        static DateTime ParseDate(string value)
        {
            DateTime date;
            if (!DateTime.TryParseExact(value, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                if (!DateTime.TryParseExact(value, "yyyy-mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    if (!DateTime.TryParseExact(value, "yyyy-mm-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        throw new Exception("WRONG FORMAT");
                    }
                }
            }

            return date;
        }

        static (DateTime, DateTime) GetMinAndMaxDate(TimelineEvent[] timeline)
        {
            var min = timeline[0].date;
            var max = timeline[0].date;

            for (int i = 0; i < timeline.Length; i++)
            {
                var date = timeline[i].date;
                if (date < min)
                    min = date;
                if (date > max)
                    max = date;
            }
            return (max, min);
        }

        static (int, int, int) DeltaMinAndMaxDate(DateTime max, DateTime min)
        {
            var m = 0;
            var y = 0;
            var day = max.Day - min.Day;
            if (day < 0)
            {
                day = day + DateTime.DaysInMonth(min.Year, min.Month);
                m = 1;
            }
            var mon = max.Month - min.Month - m;
            if (mon < 0)
            {
                mon = mon + 12;
                y = 1;
            }
            var year = max.Year - min.Year - y;
            return (year, mon, day);
        }

        static void FindPeople(Person[] people)
        {
            Console.WriteLine("Список родившихся людей в високосный год и не достигших 20 лет:");
            for ( int i =0; i < people.Length; i++)
            {
                var birthDate = people[i].birthday;
                TimeSpan live = DateTime.Now - birthDate;
                if (DateTime.IsLeapYear(birthDate.Year) && live.Days / 7305 <= 20) //7305 - кол-во дней в 20-ти годах
                {
                    Console.WriteLine(people[i].name);
                }
            }
        }
        static void TaskOne (TimelineEvent[] timelines)
        {
            (DateTime max, DateTime min) = GetMinAndMaxDate(timelines);

            (int years, int months, int days) = DeltaMinAndMaxDate(max, min);

            Console.WriteLine("Самое раннее событие: " + max);
            Console.WriteLine("Самое позднее событие: " + min);
            Console.WriteLine("Между ними прошло " + years + " лет " + months + " месяцев " + days + " дней");
        }
        static void TaskTwo(Person[] people)
        {
            FindPeople(people);
        }

        static void Main(string[] args)
        {
            //string peoplePath1 = @"C:\Users\home\OneDrive\Рабочий стол\TimeTrees\timetrees\people.csv";
            //string timelinePath1 = @"C:\Users\home\OneDrive\Рабочий стол\TimeTrees\timetrees\timeline.csv";
            string peoplePath = Path.Combine(Environment.CurrentDirectory, "people.csv");
            string timelinePath = Path.Combine(Environment.CurrentDirectory, "timeline.csv");

            Console.WriteLine("Нажмите 1, если формат файла .csv");
            Console.WriteLine("Нажмите 2, если формат файла .json");
            int choose = int.Parse(Console.ReadLine());
            Person[] peopleData = ReadPeople(peoplePath);
            TimelineEvent[] timelineData = ReadTimeline(timelinePath);
            switch (choose)
            {
                case 1:
                    peopleData = ReadPeople(peoplePath);
                    timelineData = ReadTimeline(timelinePath);
                    TaskOne(timelineData);
                    TaskTwo(peopleData);
                    break;
                case 2:
                    var json = File.ReadAllText("people.json");
                    peopleData = JsonConvert.DeserializeObject<Person[]>(json);
                    var json1 = File.ReadAllText("timeline.json");
                    timelineData = JsonConvert.DeserializeObject<TimelineEvent[]>(json1);
                    TaskOne(timelineData);
                    TaskTwo(peopleData);
                    break;
                default:
                    Console.WriteLine("Нажмите 1 или 2");
                    break;
            }

            //var json = JsonConvert.SerializeObject(peopleDataStruct);
            //File.WriteAllText("people.json", json);

            //var json1 = JsonConvert.SerializeObject(timelineDataStruct);
            //File.WriteAllText("timeline.json", json1);
        }
    }
}
